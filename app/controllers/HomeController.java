package controllers;

import play.mvc.*;

import views.html.*;

public class HomeController extends Controller {

    public static Result index() {
        return ok(index.render());
    }
    public static Result login() {
        return ok(login.render());
    }
    public static Result register() {
        return ok(register.render());
    }
    public static Result server() {
        return ok(server.render());
    }
    public static Result idc() {
        return ok(idc.render());
    }
    public static Result host() {
        return ok(host.render());
    }
    public static Result ssl() {
        return ok(ssl.render());
    }
    public static Result domain() {
        return ok(domain.render());
    }
    public static Result monitor() {
        return ok(monitor.render());
    }
    public static Result financial() {
        return ok(financial.render());
    }
    public static Result ecommerce() {
        return ok(ecommerce.render());
    }
    public static Result mobile() {
        return ok(mobile.render());
    }
    public static Result game() {
        return ok(game.render());
    }
    public static Result website() {
        return ok(website.render());
    }
    public static Result help() {
        return ok(help.render());
    }
    public static Result about() {
        return ok(about.render());
    }
    public static Result console() {
        return ok(console.render());
    }
    public static Result icp() {
        return ok(icp.render());
    }


}
