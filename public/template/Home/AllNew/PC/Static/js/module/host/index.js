$(function () {
	//获取每个模块距离顶部的距离加上自身的高度,存入数组
	//初始化的值,表示为到达这个值显示定位菜单
	var topArr = [455];
	$(".nav-hide .banner-nav li").eq(0).find("a").addClass("active");
	$(".module").each(function () {
		topArr.push($(this).offset().top + $(this).height() - 80);
	})
	$(window).scroll(function () {
		$.each(topArr, function (i) {
			if ($(document).scrollTop() <= topArr[0] || $(document).scrollTop() >= topArr[topArr.length]) {
				$(".nav-show .banner-nav li a").removeClass("active");
				$(".nav-show").hide(0);
			}
			else {
				$(".nav-show").show(0);
				if ($(document).scrollTop() >= topArr[i] && $(document).scrollTop() <= topArr[i + 1]) {
					$(".nav-show .banner-nav li a").removeClass("active");
					$(".nav-show .banner-nav li").eq(i).find("a").addClass("active");
				}
			}
		})
	})

	$(".banner-nav li a").on("click", function () {
		var index = $(this).parent("li").index()
		$(".nav-show .banner-nav li a").removeClass("active");
		$(".nav-show .banner-nav li").eq(index).find("a").addClass("active");
		$('body,html').animate({
			scrollTop: topArr[index] + 10
		}, 800)
	});

	$(".table-fold-bar").click(function () {
		var $_self = $(this);
		var foldClass = "table-fold";
		if ($_self.parent().hasClass(foldClass)) {
			$_self.parent().removeClass(foldClass);
		}
		else {
			$_self.parent().addClass(foldClass);
		}
	});

	// 让共享独享等高
	if($(".host-package-right").outerHeight()<$(".host-package-left").outerHeight()){
		$(".host-package-right").outerHeight($(".host-package-left").outerHeight());
	}else {
		$(".host-package-left").outerHeight($(".host-package-right").outerHeight());
	}

})