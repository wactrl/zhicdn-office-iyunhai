$(function () {
	// 优势滚动，有限初始化，否则下面计算元素高度时会出错
	// bxSlider初始化banner滚动
	if ($(".youshi-slider").length > 0) {
		$(".youshi-slider").bxSlider({
			slideWidth: 282,
			minSlides: 2,
			maxSlides: 4,
			slideMargin: 24
		});
	}


	//获取每个模块距离顶部的距离加上自身的高度,存入数组
	//初始化的值,表示为到达这个值显示定位菜单
	var topArr = [455];
	$(".nav-hide .banner-nav li").eq(0).find("a").addClass("active");
	$(".server-module").each(function () {
		topArr.push($(this).offset().top + $(this).height() - 20);
	});
	$(window).scroll(function () {
		$.each(topArr, function (i) {
			if ($(document).scrollTop() <= topArr[0] || $(document).scrollTop() >= topArr[topArr.length]) {
				$(".nav-show .banner-nav li a").removeClass("active");
				$(".nav-show").hide(0);
			}
			else {
				$(".nav-show").show(0);
				if ($(document).scrollTop() >= topArr[i] && $(document).scrollTop() <= topArr[i + 1]) {
					$(".nav-show .banner-nav li a").removeClass("active");
					$(".nav-show .banner-nav li").eq(i).find("a").addClass("active");
				}
			}
		})
	});

	$(".banner-nav li a").on("click", function () {
		var index = $(this).parent("li").index()
		$(".nav-show .banner-nav li a").removeClass("active");
		$(".nav-show .banner-nav li").eq(index).find("a").addClass("active");
		$('body,html').animate({
			scrollTop: topArr[index] + 10
		}, 800)
		$(".nav-show").show(0);
	});

	// 显示、关闭视频蒙层
	var $_videoMask = $(".video-mask-container");
	$(".server-video-placeholder").click(function () {
		$_videoMask.show();
	});
	$(".video-close").click(function () {
		$_videoMask.hide();
	});



	// 适应场景切换
	$(".scene-item").mouseenter(function () {
		var $_self = $(this);
		var activeClass = "scene--active";
		if (!$_self.hasClass(activeClass)) {
			$_self.addClass(activeClass).siblings().removeClass(activeClass);
		}
	});

});