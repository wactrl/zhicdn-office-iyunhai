$(function () {
	// 搜索按钮启用事件
	var $_searchBtn = $(".search-btn");
	$(".input-main").keyup(function () {
		var $_self = $(this);
		$_searchBtn.attr("disabled", ($_self.val() ? false : true));
	});

	// 菜单栏收缩事件（初始化收缩）
	if ($(".page-id").length > 0) {
		$(".help-menu dt").click(function () {
			var me = $(this);
			var $_parent = me.parent();
			if ($_parent.hasClass("active")) {
				$_parent.removeClass("active");
			} else {
				$_parent.addClass("active");
				$_parent.siblings().removeClass("active");
			}
		});
		var pageId = $(".page-id").html();
		var $_activeMenu = $("[data-id='" + pageId + "']");
		$_activeMenu.parent().addClass("active");
	}

	//动态统一帮助问题分类高度大小
	setTimeout(function () {
		var heightList = [];
		var $_listTypeItems = $('.issue-cloud-details');
		for (var i = 0; i < $_listTypeItems.length; i++) {
			heightList[i] = $_listTypeItems.eq(i).height();
		}
		var heightMax = heightList[0];
		if (heightMax < heightList[1]) {
			heightMax = heightList[1];
		}
		;
		if (heightMax < heightList[2]) {
			heightMax = heightList[2];
		}
		;
		if (heightMax < heightList[3]) {
			heightMax = heightList[3];
		}
		;
		$_listTypeItems.height(heightMax);
	}, 200);

	if ($(".help-menu").length > 0) {
		$(".tab-menu a").not(".active").click(function () {
			var contentId = $(this).data("content_id");
			$.cookie("helpIndex", contentId, {path: "/"});
			location.href = "/help/";
		})
	}

	// 判断是否首页
	if ($(".help-page-index").length > 0) {
		var contentId = $.cookie("helpIndex");
		$(".type-" + contentId).trigger("click");
		$.removeCookie("helpIndex",{path: "/"});
	}

	// 帮助中心提交反馈
	var $_useful = $("input[name=useful]");
	var $_submitContent = $(".submit-content"),
		$_submitSuccess = $(".submit-success"),
		$_submitError = $(".submit-error");

	var $_chooseBtn = $(".solved-or-not");

	var submitFunc = function(useful){
		var $_form =$("#submitForm");
		$.ajax({
			type: "post",
			url: $_form.attr("action"),
			data: $_form.serialize(),
			success: function (responseData) {
				if(responseData.result) {
					if (useful) {
						$_submitSuccess.removeClass("hide");
					} else {
						$_submitError.removeClass("hide");
					}
					$_submitContent.add($_chooseBtn).addClass("hide");
				}else{
					NY.warn(responseData.text);
				}
			},
			error: function() {
				NY.showBusy();
			}
		});
	}

	$(".btn-smile").add(".btn-cry").click(function(){
		var useful = $(this).data("value");
		$_useful.val(useful);
		if(useful == "0"){
			$_submitContent.removeClass("hide");
		}else{
			submitFunc(useful);
		}
	});
	$(".submit-btn").click(function(){
		submitFunc();
	})
});
