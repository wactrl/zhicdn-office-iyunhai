$(function () {
	// 跳转到价格
	$("#jumpToPrice").click(function () {
		$('body,html').animate({
			scrollTop: $(".price-block").offset().top
		}, 1000);
	});

	var $_checkboxArea = $(".checkbox-area");
	var checkClass = "checked";
	// 域名选中
	$_checkboxArea.on("click", ".checkbox-item", function () {
		var $_self = $(this);

		if ($_self.hasClass(checkClass)) {
			// 如果只剩下一个域名，不可取消选择
			if ($_checkboxArea.find(".checked").length == 1) {
				return false;
			}
			$_self.removeClass(checkClass);
			$_self.find("input").prop("checked", false);
		}
		else {
			$_self.addClass(checkClass);
			$_self.find("input").prop("checked", true);
		}

		// 下拉按钮文字显示
		var $_dropTrigger = $(".domain-drop-trigger");
		if ($_checkboxArea.find(".checked").length > 1) {
			$_dropTrigger.text("多后缀");
		}
		else {
			$_dropTrigger.text($_checkboxArea.find(".checked").data("name"));
		}
	});

	// 展开下拉
	$(".domain-drop-trigger").click(function (e) {
		e.preventDefault();
		e.stopPropagation();
		var $_self = $(this);
		var $_domainList = $(".domain-search-list");
		if ($_domainList.hasClass("active")) {
			$_self.removeClass("active");
			$_domainList.removeClass("active");
		}
		else {
			$_self.addClass("active");
			$_domainList.addClass("active")
		}
	});
	// 点击除了搜索区域以外的地方，隐藏下拉
	$(".domain-search-list").add(".domain-search-input").add(".domain-search-button").click(function (e) {
		e.preventDefault();
		e.stopPropagation();
	});
	$("body").click(function (e) {
		$(".domain-drop-trigger").removeClass("active");
		$(".domain-search-list").removeClass("active");
	});

	/* 域名下拉相关操作 */
	// 把常用域名保存在cookie中
	var saveCommonUseDomain = function () {
		var dArray = [];
		$(".checkbox-item").each(function () {
			var $_self = $(this);
			if ($_self.hasClass("checked")) {
				dArray.push($_self.data("name"));
			}
		});

		$.cookie("commonUseDomain", dArray.join(","), {path: "/",expires: 365});

		var $_setRemind = $(".set-remind");
		$_setRemind.show();
		setTimeout(function () {
			$_setRemind.fadeOut(200);
		}, 2000);
	};
	// 判断cookie内是否有常用域名，如果有cookie记录，按照cookie初始化，否则按照页面初始化的记录到cookie中
	var domainsArray = [];
	var setCommonDomain = function (domainString) {
		$(".checkbox-item").removeClass("checked").find("input").prop("checked", false);
		domainsArray = domainString.split(",");
		$.each(domainsArray, function (index, value) {
			$("[data-name='" + value + "']").addClass("checked").find("input").prop("checked", true);
		});
	};
	// 进入页面时判断是否有保存commonUseDomain cookie
	if ($.cookie("commonUseDomain")) {
		setCommonDomain($.cookie("commonUseDomain"));
	}
	else {
		$(".checkbox-item").each(function () {
			saveCommonUseDomain();
		});
	}

	// 域名下拉操作
	$(".operate-area").find("label").click(function () {
		var $_label = $(this);
		$_label.addClass("label-checked").siblings().removeClass("label-checked");
		$_label.find("input").prop("checked", true);
		var opType = $(this).data("op");
		if (opType == 1) {
			$_checkboxArea.find(".checkbox-item").addClass(checkClass).find("input").prop("checked", true);
		}
		else if (opType == 2) {
			$_checkboxArea.find(".checkbox-item").removeClass(checkClass).find("input").prop("checked", false);
		}
		else {
			setCommonDomain($.cookie("commonUseDomain"));
		}

	});
	$(".view-more").click(function () {
		window.location.href = $(this).attr("href");
	});


	// 保存常用按钮
	$(".set-common-use").click(function () {
		saveCommonUseDomain();
	});

	// 域名查询按钮正则判断内容方法
	var isDomainContentRegOk = function (value) {
		var reg = /^([\u4e00-\u9fa5\w-\.]+)$/;
		return reg.test(value);
	};

	var showDomainErrorDialog = function () {
		$.dialog({
			title: "温馨提示",
			icon: "alert",
			padding: "20px 25px 20px 10px",
			content: "当前域名格式输入错误，只支持中文、英文、数字以及连字符\"-\"（即中横线）",
			ok: true,
			okVal: "确定"
		});
	};

	$("#domainIndexSubmit").click(function () {
		var $_domainSearchInput = $(".domain-search-input");
		if ($_domainSearchInput.val() && !isDomainContentRegOk($_domainSearchInput.val())) {
			showDomainErrorDialog();
			return false;
		}

		$(this).parents("form").submit();
	});

	// 推荐域名滚动
	if ($(".rec-slider").length > 0) {
		$(".rec-slider").bxSlider({
			slideWidth: 276,
			minSlides: 2,
			maxSlides: 4,
			slideMargin: 12,
			auto: true,
			autoHover: true,
			pause: 8000,
			touchEnabled: false
		});
	}

	// 域名价格切换
	$(".price-deck-trigger").click(function () {
		var $_parent = $(this).parent();
		if ($_parent.hasClass("active")) {
			$_parent.removeClass("active");
		}
		else {
			$_parent.addClass("active");
		}
	});

	/* 搜索结果页 */
	if ($(".domain-search-body").length > 0) {
		// 搜索方式切换
		$("#searchType").on('click', 'li', function() {
			var $_this = $(this);
			var index = $_this.data('index');
			if(!$_this.hasClass('active')) {
				$_this.addClass('active').siblings().removeClass('active');
				$(".search-content").find('.input-panel').eq(index).addClass('active').siblings().removeClass('active');
			}
		});

		// 进入页面选中参数
		$(".checkbox-item").removeClass("checked").find("input").prop("checked", false);
		$.each(suffixArray, function (index, value) {
			$("[data-name='" + value + "']").addClass("checked").find("input").prop("checked", true);
		});
		// 如果没有传值进来，则选中常用
		if (suffixArray.length == 0) {
			$(".operate-area").find("label").eq(2).trigger("click");
		}

		// 根据来源页面的search情况，进行异步查询
		var $_domainInput = $("#domainInput");
		var $_resultOuter = $("#resultOuter");
		var $_searchButton = $("#domainSearchButton")
		var $_resultRowTpl = $("#resultRowTpl");
		var searchKey = $_domainInput.val();
		// 获取后缀价格
		var getSuffixPrice = function (suffix, $row) {
			$row.find(".result-price-loading").show();
			if (priceObj) {
				$row.find(".result-price-loading").hide();
				$row.find(".result-price").show();
				$row.find(".result-price-inner").html(priceObj[suffix]);
			}
			else {
				$.ajax({
					type: "post",
					dataType: "json",
					url: "/domain/price",
					data: {
						suffix: suffix,
						year: 1
					},
					success: function (data) {
						if (data.result) {
							$row.find(".result-price-loading").hide();
							$row.find(".result-price").show();
							$row.find(".result-price-inner").html(data.price);
						}
						else {

						}
					}
				});
			}

		};
		// 获取域名状态
		var getDomainState = function (domain, suffix) {
			// 填充搜索项目
			var $_thisRow = $_resultRowTpl.find(".result-row ").clone();
			$_resultOuter.append($_thisRow);
			$_thisRow.find(".result-name").html(domain);
			$_thisRow.find(".reg-button").data("domain", domain);
			$_thisRow.find(".reg-button").data("suffix", suffix);
			$.ajax({
				type: "post",
				dataType: "json",
				url: "/domain/domainStatus",
				data: {
					domain: domain,
					suffix: suffix
				},
				success: function (data) {
					$_thisRow.removeClass("result-loading");
					if (data.result) {
						$_thisRow.addClass("result-success");
						$_thisRow.find(".result-state").html("（" + data.text + "）");
						$_thisRow.find(".result-description").html(data.description);
						// 查询价格
						getSuffixPrice(suffix, $_thisRow);
					}
					else {
						// 如果code是 -1 ，则为API报错
						if (data.code == -1) {
							$_thisRow.addClass("result-error");
							$_thisRow.find(".result-error-reminder").html(data.text);
						}
						else {
							$_thisRow.addClass("result-false");
							$_thisRow.find(".result-state").html("（" + data.text + "）");
						}
					}

					// 判断当前查询情况，如果查询完了就解禁按钮
					if ($_resultOuter.find(".result-loading").length == 0) {
						$_searchButton.val("查询域名").prop("disabled", false);
					}
				}
			});
		};
		// 搜索方法
		var goSearchDomain = function (domain) {
			$_resultOuter.html("");
			$_searchButton.val("查询中").prop("disabled", true);
			var pureDomain = domain.split(".")[0];
			$(".checkbox-area").find(".checked").each(function () {
				var joinedDomain = pureDomain + $(this).data("name");
				getDomainState(joinedDomain, $(this).data("name"));
			});
		};

		// 页面加载如果有搜索值，则自动请求
		if (searchKey && $(".checkbox-area").find(".checked").length > 0) {
			goSearchDomain(searchKey);
		}

		// 点击搜索按钮请求
		$_searchButton.click(function () {
			if ($_domainInput.val() && $(".checkbox-area").find(".checked").length > 0) {
				// 判断输入的内容
				var inputContentBeforeDot = $_domainInput.val().split(".")[0];
				if (!isDomainContentRegOk(inputContentBeforeDot)) {
					showDomainErrorDialog()
					return false;
				}

				goSearchDomain($_domainInput.val());
			}
			else if (!$_domainInput.val())  {
				NY.warn("请输入你想要查找的域名", 3, function () {
					$_domainInput.focus()
				});
			}
			else if ($(".checkbox-area").find(".checked").length == 0) {
				NY.warn("请选择要查询的域名后缀", 3);
			}
		});
		// 监听回车查询时间
		$_domainInput.keydown(function (e) {
			if (e.keyCode == 13 && !$_searchButton.prop("disabled")) {
				$_searchButton.trigger("click");
			}
		});

		// 注册（加入购物车，暂时功能上立即注册等同于加入购物车）
		$_resultOuter.on("click", ".reg-button", function () {
			var $_self = $(this);
			NY.loginCheckThenDo(function () {
				$.ajax({
					type: "post",
					dataType: "json",
					url: "/domain/addCart",
					data: {
						domain: $_self.data("domain"),
						suffix: $_self.data("suffix")
					},
					success: function (data) {
						if (data.result) {
							window.location.href = "/domain/cart.html";
						}
						else {
							NY.warn(data.text);
						}
					}
				});
			});

		});

	}
});