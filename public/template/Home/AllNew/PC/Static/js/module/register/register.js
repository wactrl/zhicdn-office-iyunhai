$(function () {
	
	$("#agreementCheckbox").change(function () {
		var checkboxTrigger = $("#checkboxTrigger");
		var submitBtn = $("#submitBtn");
		if ($(this).prop("checked")) {
			checkboxTrigger.addClass("checkbox-icon-checked");
			submitBtn.addClass("disabled").attr("disabled", true);
		} else {
			checkboxTrigger.removeClass("checkbox-icon-checked");
			submitBtn.removeClass("disabled").attr("disabled", false);
		}
	});

	//表单验证
	// 获取密码范围
	var passwordMin = parseInt($("#minPwd").html()) || 8;
	var passwordMax = parseInt($("#maxPwd").html()) || 30;
	var config = {
		passwordMin:passwordMin,
		passwordMax:passwordMax
	};
	var action = $("#register").attr('action');
	$("#register").attr('action', action + '?random_num=' + (randomNum(8) + '_' + (+new Date)));
	$.formValidator.initConfig({
		formID: "register", debug: false, submitOnce: true, onSuccess: function () {
			$(".reg-submit").attr("disabled", true);
			$(".reg-submit").addClass("disabled");
			var loadingTips = NY.loading("正在提交，请稍后……", 60);

			$("#register").ajaxSubmit({
				success: function (data) {
					$("#register").attr('action', action + '?random_num=' + (randomNum(8) + '_' + (+new Date)));
					loadingTips.close();
					var json = (typeof data == "string") ? eval('(' + data + ')') : data;
					if (json.result) {
						NY.success(json.text, 1, function () {
							window.location = '/register/profile.html';
						});
					} else {
						NY.warn(json.text, 2, function () {
							$("#submitBtn").attr("disabled", false);
							$(".reg-submit").addClass("disabled");
						});
					}
				}
			});

		},
		submitAfterAjaxPrompt: '有数据正在异步验证，请稍等...'
	});
	$("#mobile").formValidator({
		onShow: "手机号码可用于登录、激活账号、密码找回",
		onFocus: "手机号码可用于登录、激活账号、密码找回",
		onCorrect: "填写正确"
	}).inputValidator({min: 11, max: 11, onError: "手机号码填写错误"}).regexValidator({
		regExp: "^1[34578][0-9]{9}$",
		onError: "你输入的手机号码不正确"
	}).ajaxValidator({
		dataType: "json",
		async: false,
		//手机号码是否可用验证接口
		url: "/register/ajaxcheckmobile.html",
		success: function (data) {
			if (data.result) {
				return true;
			} else {
				return false;
			}
			return false;
		},
		error: function (jqXHR, textStatus, errorThrown) {
			NY.showBusy();
		},
		onError: function () {
			return "该手机号码不可用";
		},
		onWait: "请稍候..."
	});
	$("#password").formValidator({
		onShow: "密码由" + config.passwordMin + "-" + config.passwordMax + "个英文字母、数字和特殊符号组成",
		onFocus: "密码由" + config.passwordMin + "-" + config.passwordMax + "个英文字母、数字和特殊符号组成",
		onCorrect: "密码格式填写正确"
	}).inputValidator({min: config.passwordMin, max: config.passwordMax, onError: "密码格式填写错误"}).regexValidator({
		regExp: "[A-Za-z].*[0-9]|[0-9].*[A-Za-z]",
		onError: "你输入的密码格式不正确"
	});
	$("#password2").formValidator({onShow: "请再次输入密码", onFocus: "请再次输入密码", onCorrect: "填写正确"}).inputValidator({
		min: 1,
		onError: "请再次输入密码"
	}).compareValidator({desID: "password", operateor: "=", onError: "两次输入的密码不一致"});
	$("#mobilecode").formValidator({onShow:"请输入收到的6位验证码",onFocus:"请输入收到的6位验证码",onCorrect:"验证码填写正确"}).inputValidator({min:6,max:6,onError:"验证码填写错误"}).ajaxValidator({
		dataType : "json",
		async : false,
		//手机验证码接口
		url : "/register/ajaxcheckmobilecode.html",
		success : function(data){
			if(data.result){
				return true;
			}else{
				return false;
			}
			return false;
		},
		buttons: $("#button"),
		error: function(jqXHR, textStatus, errorThrown){alert("服务器没有返回数据！");},
		onError : "验证码错误",
		onWait : "请稍候..."
	});

	var sendCodeBtn = $("#sendcode");
	// 验证码发送后倒计方法
	var sendCountdown = function(countdownTime){

		if(countdownTime > 0){
			sendCodeBtn.attr("disabled", true);
			sendCodeBtn.addClass("send-captcha-disabled");
			sendCodeBtn.val((countdownTime--) + "秒后重新发送");
			var timer = setTimeout(function(){
				sendCountdown(countdownTime);
			},1000);

		}else{
			sendCodeBtn.attr("disabled", false);
			sendCodeBtn.removeClass("send-captcha-disabled");
			sendCodeBtn.val("点击再次发送");
			countdownTime = 60;
		}
	};
	var sendtime = $("#sendtime").val();

	//当剩余时间大于0时页面保持倒计时
	if(sendtime > 0) {
		sendCountdown(sendtime);
	}
	sendCodeBtn.click(function(){
		if (!$("#mobileTip").hasClass("onCorrect")){
			NY.warn("请先填写您的手机号码",1.5);
			return false;
		}else{
			// 刷新验证码
			$(".show-captcha").trigger("click");
			// 验证码弹窗
			var inputCaptchaBox = $.dialog({
				content:$("#captchaCheck")[0],
				title:'请输入验证码',
				width: 380,
				height: 110,
				okVal:'确认发送',
				ok:function(){
					var captchaCodeVal = $("#captcha").val();
					var mobileNum = $("#mobile").val();
					$.ajax({
						//验证码验证接口
						url:"/register/sendMobileCode.html?mobile=" + mobileNum + "&captcha=" + captchaCodeVal,
						cache:false,
						dataType:"json",
						success:function(data){
							//如果验证码正确
							if(data.result){
								inputCaptchaBox.close();
								NY.success(data.text, 5);
								// 验证码发送后倒计时60s
								sendCountdown(60);
							}else{
								NY.warn(data.text, 3, function () {
									// 刷新验证码
									$(".show-captcha").trigger("click");
								});

							}
						},
						error:function(){
							NY.showBusy();
						}
					});
					return false;
				},
				cancel:true
			});
		}
	});
});

