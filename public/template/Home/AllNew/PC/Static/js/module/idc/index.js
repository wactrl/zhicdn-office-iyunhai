$(function () {
	//获取每个模块距离顶部的距离加上自身的高度,存入数组
	//初始化的值,表示为到达这个值显示定位菜单
	var topArr = [455];
	$(".nav-hide .banner-nav li").eq(0).find("a").addClass("active");
	$(".module").each(function (i) {
		//由于有误差,增加280作为误差补偿
		if(i==0){
			topArr.push($(this).offset().top + $(this).height()+280);
		}else {
			topArr.push($(this).offset().top+ $(this).height()+50);
		}

	});
	$(window).scroll(function () {
		$.each(topArr, function (i) {
			if ($(document).scrollTop() <= topArr[0] || $(document).scrollTop() >= topArr[topArr.length]) {
				$(".nav-show .banner-nav li a").removeClass("active");
				$(".nav-show").hide(0);
			}
			else {
				$(".nav-show").show(0);
				if ($(document).scrollTop() >= topArr[i] && $(document).scrollTop() <= topArr[i + 1]) {
					$(".nav-show .banner-nav li a").removeClass("active");
					$(".nav-show .banner-nav li").eq(i).find("a").addClass("active");
				}
			}
		})
	});

	$(".banner-nav li a").on("click", function () {
		var index = $(this).parent("li").index()
		$(".nav-show .banner-nav li a").removeClass("active");
		$(".nav-show .banner-nav li").eq(index).find("a").addClass("active");
		$('body,html').animate({
			scrollTop: topArr[index] + 10
		}, 800)
	});

	// 机房介绍切换
	$(".dc-tabs").on("click", "li", function () {
		var $_self = $(this);
		$_self.addClass("tab--active").siblings().removeClass("tab--active");
		$(".dc-block").hide().each(function () {
			if ($(this).attr("id") == ("dcBlock" + $_self.data("tab"))) {
				$(this).show();
			}
		});
	}).find("li:first-child").trigger("click");
	// 产品切换
	$(".products-tabs").on("click", "li", function () {
		var $_self = $(this);
		$_self.addClass("tab--active").siblings().removeClass("tab--active");
		$(".type-table").hide().each(function () {
			if ($(this).attr("id") == ("produceTable" + $_self.data("tab"))) {
				$(this).show();
			}
		});
	}).find("li:first-child").trigger("click");
});